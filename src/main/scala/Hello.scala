import com.twitter.finagle.{Http, Service}
import com.twitter.finagle.http
import com.twitter.server.TwitterServer
import com.twitter.util.{Await, Future}

object Hello extends TwitterServer {
  val service = new Service[http.Request, http.Response] {
    def apply(req: http.Request): Future[http.Response] = {
      val response = http.Response(req.version, http.Status.Ok)
      logger.error("BOOM!")
      response.setContentString("Hello World")
      Future.value(response)
    }
  }

  def main(): Unit = {
    val server = Http.serve(":8080", service)
    onExit {
      server.close()
    }
    Await.ready(server)
  }
}
