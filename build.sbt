ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.8"

lazy val root = (project in file("."))
  .settings(
    name := "webby"
  )

libraryDependencies ++= Seq(
  "com.twitter" %% "finagle-http" % "22.4.0",
  "com.twitter" %% "twitter-server" % "22.4.0",
  "ch.qos.logback" % "logback-classic" % "1.2.11",
  "co.elastic.logging" % "logback-ecs-encoder" % "1.4.0"
)